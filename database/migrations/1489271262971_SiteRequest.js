'use strict'

const Schema = use('Schema')

class SiteRequestTableSchema extends Schema {

  up () {
    this.create('site_requests', (table) => {
      table.increments()
      table.string('fio').nullable()
      table.string('phone').nullable()
      table.string('email').nullable()
      table.string('org_name').nullable()
      table.string('full_org_name').nullable()
      table.string('short_org_name').nullable()
      table.string('email_org').nullable()
      table.string('address_org').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('site_requests')
  }

}

module.exports = SiteRequestTableSchema
