'use strict'

const Schema = use('Schema')

class HelloTableTableSchema extends Schema {

  up () {
    this.create('hello_table', (table) => {
      table.increments()
      table.string('name')
      table.timestamps()
    })
  }

  down () {
    this.drop('hello_table')
  }

}

module.exports = HelloTableTableSchema
