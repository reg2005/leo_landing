'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.group('version1', function () {
  Route.post('site.form.save/step1', 'SiteFormStepOneController.save')
  Route.get('get.csrf.token', function * (request, response) {
    response.send(
      request.csrfToken()
    )
  })
}).prefix('api/v1')

Route.any('*', 'NuxtController.render')
