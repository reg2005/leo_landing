'use strict'
const SiteRequest = use('App/Model/SiteRequest')

class SiteFormStepOneController {
  * save (request, response) {

    response.ok({success:true})
    console.log(request.all())
    const post = new SiteRequest()

    post.fill(
      request.all()
    )

    let id = yield post.save() // SQL Update
    // response.ok(post)

  }

}

module.exports = SiteFormStepOneController
