'use strict'

const resolve = require('path').resolve

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Adonuxt',
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Adonuxt project'
      }
    ],
    link: [
      {rel: 'icon', type: 'shortcut icon', href: '/assets/ico/favicon.png'},
      {rel: 'apple-touch-icon-precomposed', sizes: '144x144', href: '/assets/ico/apple-touch-icon-144-precomposed.png'},
      {rel: 'apple-touch-icon-precomposed', sizes: '114x114', href: '/assets/ico/apple-touch-icon-114-precomposed.png'},
      {rel: 'apple-touch-icon-precomposed', sizes: '72x72', href: '/assets/ico/apple-touch-icon-72-precomposed.png'},
      {rel: 'apple-touch-icon-precomposed', href: '/assets/ico/apple-touch-icon-57-precomposed.png'},

      { rel: 'stylesheet', href: 'http://fonts.googleapis.com/css?family=Roboto:400,100,300,500' },
      { rel: 'stylesheet', href: '/assets/bootstrap/css/bootstrap.min.css' },
      { rel: 'stylesheet', href: '/assets/typicons/typicons.min.css' },
      { rel: 'stylesheet', href: '/assets/css/animate.css' },
      { rel: 'stylesheet', href: '/assets/css/form-elements.css' },
      { rel: 'stylesheet', href: '/assets/css/style.css' },
      { rel: 'stylesheet', href: '/assets/css/media-queries.css' },
      { rel: 'stylesheet', href: '/assets/css/style.css' },
      { rel: 'stylesheet', href: 'https://unpkg.com/element-ui/lib/theme-default/index.css' },
    ],
    script: [
      {src: '/assets/js/jquery-1.11.1.min.js'},
      {src: '/assets/bootstrap/js/bootstrap.min.js'},
      {src: '/assets/js/jquery.backstretch.min.js'},
      {src: '/assets/js/wow.min.js'},
      {src: '/assets/js/retina-1.1.0.min.js'},
      // {src: '/assets/js/scripts.js'},
      {src: '/assets/js/placeholder.js'},
    ]
  },
  plugins: ['~plugins/all', '~plugins/axios'],
  // build: {
  //   vendor: ['vue-masked-input']
  // },
  /*
  ** Global CSS
  */
  // css: ['~assets/css/main.css'],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#744d82' },
  router: {
    scrollBehavior: function (to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition
      } else {
        return { x: 0, y: 0 }
      }
    }
  },
  /*
  ** Point to resources
  */
  srcDir: resolve(__dirname, '..', 'resources')
}
