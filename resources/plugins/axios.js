/**
 * Created by evgeniy on 12.03.17.
 */
import Vue from 'vue'
import axios from 'axios'

const axiosEx = axios.create({
  baseURL: '/api/v1',
  withCredentials: true,
})

import VueAxios from 'vue-axios'

Vue.use(VueAxios, axiosEx)

export default axiosEx
