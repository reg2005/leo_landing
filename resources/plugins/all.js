/**
 * Created by evgeniy on 12.03.17.
 */
import Vue from 'vue'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'

Vue.use(ElementUI, {locale})
import VueMask from 'v-mask'

/** Activate vue.js plugins **/
Vue.use(VueMask);
