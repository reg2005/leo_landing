export const setFio = (state, payload) => {
    state.form.fio = payload
}
export const setPhone = (state, payload) => {
    state.form.phone = payload
}
export const setEmail = (state, payload) => {
    state.form.email = payload
}
export const setOrgName = (state, payload) => {
    state.form.orgName = payload
}
