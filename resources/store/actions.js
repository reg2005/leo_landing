import axios from '~plugins/axios'

export const nuxtServerInit = ({commit, dispatch}, {req, route}) => {
    return Promise.all([
        // dispatch('getCsrfToken')
    ])
}

export const getCsrfToken = ({commit, state}) => {
    return axios.get('get.csrf.token').then((response) => {
        var data = response.data
        commit('setLastEvent', data.lastEvent)
        commit('setSlider', data.slider)
        commit('setGallery', data.gallery)
        commit('setCategories', data.categories)
    }).catch((error) => {
        console.log(error)
    })
}

export const getAllPublicState = ({commit, state}) => {
    return axios.get('public/get.all.state').then((response) => {
        var data = response.data
        commit('setLastEvent', data.lastEvent)
        commit('setSlider', data.slider)
        commit('setGallery', data.gallery)
        commit('setCategories', data.categories)
    }).catch((error) => {
        console.log(error)
    })
}

