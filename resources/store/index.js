import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    form: {
      fio: '',
      phone: '',
      email: '',
      orgName: '',
    },
    orgCardId: null
  },
  actions,
  mutations,
  getters,
  // strict: process.env.NODE_ENV !== 'production',
  plugins: [createPersistedState()]
})

export default store
