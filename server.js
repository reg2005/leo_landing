'use strict'

/*
 |--------------------------------------------------------------------------
 | Http Server
 |--------------------------------------------------------------------------
 |
 | Here we boot the HTTP Server by calling the exported method. A callback
 | function is optionally passed which is executed, once the HTTP server
 | is running.
 |
 */

const cluster = require('cluster');
const numCPUs = require('os').cpus().length
const http = require('./bootstrap/http')

// if (process.env.NODE_ENV === 'production') {
//   if (cluster.isMaster) {
//     console.log(`Master ${process.pid} is running`);
//
//     // Fork workers.
//     for (let i = 0; i < numCPUs; i++) {
//       cluster.fork();
//     }
//
//     cluster.on('exit', (worker, code, signal) => {
//       console.log(`worker ${worker.process.pid} died`);
//     });
//   } else {
    // Workers can share any TCP connection
    // In this case it is an HTTP server

    runServer()

    // console.log(`Worker ${process.pid} started`);
//   }
// }else{
//   runServer()
// }

function runServer() {
  http(function () {
    use('Event').fire('Http.start')

    // Start nuxt.js build as soon as possible
    use('App/Http/Controllers/NuxtController')
  })
}

